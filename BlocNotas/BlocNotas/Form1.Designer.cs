﻿
namespace BlocNotas
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deshacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rehacerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cortarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pegarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cortarTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.copiarTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.pegarTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.FuenteTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.colorTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.busquedaTB = new System.Windows.Forms.ToolStripTextBox();
            this.bucquedaTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.atrasTSB = new System.Windows.Forms.ToolStripButton();
            this.adelanteTSB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tssLbl = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLblLineaNum = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLblc = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLblColumNum = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.panelRichTb = new System.Windows.Forms.Panel();
            this.ritchTB = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.panelRichTb.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.editarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(872, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.abrirToolStripMenuItem,
            this.toolStripSeparator2,
            this.guardarToolStripMenuItem,
            this.toolStripSeparator3,
            this.toolStripSeparator4,
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.add;
            this.nuevoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.nuevoToolStripMenuItem.Text = "&Nuevo";
            this.nuevoToolStripMenuItem.Click += new System.EventHandler(this.nuevoToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("abrirToolStripMenuItem.Image")));
            this.abrirToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.abrirToolStripMenuItem.Text = "&Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(153, 6);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("guardarToolStripMenuItem.Image")));
            this.guardarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.guardarToolStripMenuItem.Text = "&Guardar";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.guardarToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(153, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(153, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.logout;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.salirToolStripMenuItem.Text = "&Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // editarToolStripMenuItem
            // 
            this.editarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deshacerToolStripMenuItem,
            this.rehacerToolStripMenuItem,
            this.toolStripSeparator5,
            this.cortarToolStripMenuItem,
            this.copiarToolStripMenuItem,
            this.pegarToolStripMenuItem,
            this.toolStripSeparator6});
            this.editarToolStripMenuItem.Name = "editarToolStripMenuItem";
            this.editarToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.editarToolStripMenuItem.Text = "&Editar";
            // 
            // deshacerToolStripMenuItem
            // 
            this.deshacerToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.pasoAtras;
            this.deshacerToolStripMenuItem.Name = "deshacerToolStripMenuItem";
            this.deshacerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.deshacerToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.deshacerToolStripMenuItem.Text = "&Deshacer";
            this.deshacerToolStripMenuItem.Click += new System.EventHandler(this.deshacerToolStripMenuItem_Click);
            // 
            // rehacerToolStripMenuItem
            // 
            this.rehacerToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.pasoAdelante;
            this.rehacerToolStripMenuItem.Name = "rehacerToolStripMenuItem";
            this.rehacerToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.rehacerToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.rehacerToolStripMenuItem.Text = "&Rehacer";
            this.rehacerToolStripMenuItem.Click += new System.EventHandler(this.rehacerToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(160, 6);
            // 
            // cortarToolStripMenuItem
            // 
            this.cortarToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.Cut321;
            this.cortarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cortarToolStripMenuItem.Name = "cortarToolStripMenuItem";
            this.cortarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cortarToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.cortarToolStripMenuItem.Text = "Cor&tar";
            this.cortarToolStripMenuItem.Click += new System.EventHandler(this.cortarToolStripMenuItem_Click);
            // 
            // copiarToolStripMenuItem
            // 
            this.copiarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copiarToolStripMenuItem.Image")));
            this.copiarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copiarToolStripMenuItem.Name = "copiarToolStripMenuItem";
            this.copiarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copiarToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.copiarToolStripMenuItem.Text = "&Copiar";
            this.copiarToolStripMenuItem.Click += new System.EventHandler(this.copiarToolStripMenuItem_Click);
            // 
            // pegarToolStripMenuItem
            // 
            this.pegarToolStripMenuItem.Image = global::BlocNotas.Properties.Resources.paste;
            this.pegarToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pegarToolStripMenuItem.Name = "pegarToolStripMenuItem";
            this.pegarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pegarToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.pegarToolStripMenuItem.Text = "&Pegar";
            this.pegarToolStripMenuItem.Click += new System.EventHandler(this.pegarToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(160, 6);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cortarTSB,
            this.toolStripSeparator1,
            this.copiarTSB,
            this.toolStripSeparator9,
            this.pegarTSB,
            this.toolStripSeparator8,
            this.FuenteTSB,
            this.toolStripSeparator,
            this.colorTSB,
            this.toolStripSeparator10,
            this.busquedaTB,
            this.bucquedaTSB,
            this.toolStripSeparator11,
            this.atrasTSB,
            this.adelanteTSB,
            this.toolStripSeparator12});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(872, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cortarTSB
            // 
            this.cortarTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cortarTSB.Image = global::BlocNotas.Properties.Resources.Cut32;
            this.cortarTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cortarTSB.Name = "cortarTSB";
            this.cortarTSB.Size = new System.Drawing.Size(23, 22);
            this.cortarTSB.Text = "Cort&ar";
            this.cortarTSB.Click += new System.EventHandler(this.cortarTSB_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // copiarTSB
            // 
            this.copiarTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copiarTSB.Image = ((System.Drawing.Image)(resources.GetObject("copiarTSB.Image")));
            this.copiarTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copiarTSB.Name = "copiarTSB";
            this.copiarTSB.Size = new System.Drawing.Size(23, 22);
            this.copiarTSB.Text = "Copiar";
            this.copiarTSB.Click += new System.EventHandler(this.copiarTSB_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 25);
            // 
            // pegarTSB
            // 
            this.pegarTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pegarTSB.Image = global::BlocNotas.Properties.Resources.paste;
            this.pegarTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pegarTSB.Name = "pegarTSB";
            this.pegarTSB.Size = new System.Drawing.Size(23, 22);
            this.pegarTSB.Text = "Pegar";
            this.pegarTSB.Click += new System.EventHandler(this.pegarTSB_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // FuenteTSB
            // 
            this.FuenteTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.FuenteTSB.Image = global::BlocNotas.Properties.Resources.Font32;
            this.FuenteTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.FuenteTSB.Name = "FuenteTSB";
            this.FuenteTSB.Size = new System.Drawing.Size(23, 22);
            this.FuenteTSB.Text = "Fuente";
            this.FuenteTSB.Click += new System.EventHandler(this.FuenteTSB_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // colorTSB
            // 
            this.colorTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.colorTSB.Image = global::BlocNotas.Properties.Resources.color_circle;
            this.colorTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colorTSB.Name = "colorTSB";
            this.colorTSB.Size = new System.Drawing.Size(23, 22);
            this.colorTSB.Text = "Color";
            this.colorTSB.Click += new System.EventHandler(this.colorTSB_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 25);
            // 
            // busquedaTB
            // 
            this.busquedaTB.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.busquedaTB.Name = "busquedaTB";
            this.busquedaTB.Size = new System.Drawing.Size(100, 25);
            // 
            // bucquedaTSB
            // 
            this.bucquedaTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bucquedaTSB.Image = global::BlocNotas.Properties.Resources.lupa;
            this.bucquedaTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bucquedaTSB.Name = "bucquedaTSB";
            this.bucquedaTSB.Size = new System.Drawing.Size(23, 22);
            this.bucquedaTSB.Text = "Buscar";
            this.bucquedaTSB.Click += new System.EventHandler(this.bucquedaTSB_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 25);
            // 
            // atrasTSB
            // 
            this.atrasTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.atrasTSB.Image = global::BlocNotas.Properties.Resources.pasoAtras;
            this.atrasTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.atrasTSB.Name = "atrasTSB";
            this.atrasTSB.Size = new System.Drawing.Size(23, 22);
            this.atrasTSB.Text = "Paso Atrás";
            this.atrasTSB.Click += new System.EventHandler(this.atrasTSB_Click);
            // 
            // adelanteTSB
            // 
            this.adelanteTSB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.adelanteTSB.Image = global::BlocNotas.Properties.Resources.pasoAdelante;
            this.adelanteTSB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.adelanteTSB.Name = "adelanteTSB";
            this.adelanteTSB.Size = new System.Drawing.Size(23, 22);
            this.adelanteTSB.Text = "Paso Adelante";
            this.adelanteTSB.Click += new System.EventHandler(this.adelanteTSB_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 25);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssLbl,
            this.tssLblLineaNum,
            this.tssLblc,
            this.tssLblColumNum});
            this.statusStrip.Location = new System.Drawing.Point(0, 357);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(872, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // tssLbl
            // 
            this.tssLbl.Name = "tssLbl";
            this.tssLbl.Size = new System.Drawing.Size(118, 17);
            this.tssLbl.Text = "toolStripStatusLabel1";
            // 
            // tssLblLineaNum
            // 
            this.tssLblLineaNum.Name = "tssLblLineaNum";
            this.tssLblLineaNum.Size = new System.Drawing.Size(118, 17);
            this.tssLblLineaNum.Text = "toolStripStatusLabel1";
            // 
            // tssLblc
            // 
            this.tssLblc.Name = "tssLblc";
            this.tssLblc.Size = new System.Drawing.Size(118, 17);
            this.tssLblc.Text = "toolStripStatusLabel1";
            // 
            // tssLblColumNum
            // 
            this.tssLblColumNum.Name = "tssLblColumNum";
            this.tssLblColumNum.Size = new System.Drawing.Size(118, 17);
            this.tssLblColumNum.Text = "toolStripStatusLabel2";
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.toolStrip1);
            this.panelMenu.Controls.Add(this.menuStrip1);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(872, 49);
            this.panelMenu.TabIndex = 4;
            // 
            // panelRichTb
            // 
            this.panelRichTb.Controls.Add(this.ritchTB);
            this.panelRichTb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRichTb.Location = new System.Drawing.Point(0, 49);
            this.panelRichTb.Name = "panelRichTb";
            this.panelRichTb.Size = new System.Drawing.Size(872, 308);
            this.panelRichTb.TabIndex = 5;
            // 
            // ritchTB
            // 
            this.ritchTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ritchTB.Location = new System.Drawing.Point(0, 0);
            this.ritchTB.Name = "ritchTB";
            this.ritchTB.Size = new System.Drawing.Size(872, 308);
            this.ritchTB.TabIndex = 0;
            this.ritchTB.Text = "";
            this.ritchTB.TextChanged += new System.EventHandler(this.ritchTB_TextChanged);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(872, 379);
            this.Controls.Add(this.panelRichTb);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipal";
            this.Text = "Bloc de Notas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPrincipal_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.panelRichTb.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deshacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rehacerToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem cortarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pegarToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton pegarTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton cortarTSB;
        private System.Windows.Forms.ToolStripButton copiarTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton FuenteTSB;
        private System.Windows.Forms.ToolStripButton colorTSB;
        private System.Windows.Forms.ToolStripTextBox busquedaTB;
        private System.Windows.Forms.ToolStripButton bucquedaTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton atrasTSB;
        private System.Windows.Forms.ToolStripButton adelanteTSB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripStatusLabel tssLbl;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Panel panelRichTb;
        private System.Windows.Forms.RichTextBox ritchTB;
        private System.Windows.Forms.ToolStripStatusLabel tssLblLineaNum;
        private System.Windows.Forms.ToolStripStatusLabel tssLblc;
        private System.Windows.Forms.ToolStripStatusLabel tssLblColumNum;
    }
}

