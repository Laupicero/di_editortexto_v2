﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// En esta clase hemos creado a 2 paneles que encapsularan y dividiran la ventana por partes.
/// Para así tener una mejor estructura de la aplicación
/// 1-Panel: Ambos Menus (MenuStrip y ToolStrip)
/// 2-Panel: Encapsula el RitchTextBox
/// </summary>
namespace BlocNotas
{
    public partial class FormPrincipal : Form
    {
        //Nos servirán para contar las palabras en la función de búsqueda
        private int contadorIndice;
        private int contadorPalabras;


        //Cosntructor
        public FormPrincipal()
        {
            InitializeComponent();
            this.contadorIndice = 0;
            this.contadorPalabras = 0;
            //Para controlar la líne y las columnas del rictTextBox
            controlarLineaRitchTextBox_DeStatusBar();
        }

        // ---------------------------------------
        // Funciones de MenuStrip
        // ---------------------------------------

        //TODO: Lo añadiremos a la próxima versión
        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Disponible en la próxima versión");
        }

        //TODO: Lo añadiremos a la próxima versión
        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Disponible en la próxima versión");
        }

        //TODO: Lo añadiremos a la próxima versión
        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Disponible en la próxima versión");
        }

        //Saldremos de la Aplicación
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            preguntarAntesDeCerrar();
        }
        

        //Paso Atrás
        private void deshacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ritchTB.Undo();
        }

        //Paso adelante
        private void rehacerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ritchTB.Redo();
        }

        //Cortar
        private void cortarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ritchTB.Cut();
        }

        //Copiar
        private void copiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ritchTB.Copy();
        }

        //Pegar
        private void pegarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ritchTB.Paste();
        }

        // ---------------------------------------
        // ToolStrip
        // ---------------------------------------

        // Función Cortar
        private void cortarTSB_Click(object sender, EventArgs e)
        {
            ritchTB.Cut();
        }

        // Función Copiar
        private void copiarTSB_Click(object sender, EventArgs e)
        {
            ritchTB.Copy();
        }

        // Función Pegar
        private void pegarTSB_Click(object sender, EventArgs e)
        {
            ritchTB.Paste();
        }

        // Función Cambio de Fuente con color incluído
        // Vale tanto para el texto seleccionado como en 'global' por si no hay nada seleccionado
        private void FuenteTSB_Click(object sender, EventArgs e)
        {
            FontDialog f1 = new FontDialog();
            f1.ShowColor = true;

            if(ritchTB.SelectionLength > 0)
            {
                if (f1.ShowDialog() == DialogResult.OK)
                {
                    ritchTB.SelectionFont = f1.Font;
                    ritchTB.SelectionColor = f1.Color;
                }
            }
            else
            {
                if (f1.ShowDialog() == DialogResult.OK)
                {
                    ritchTB.Font = f1.Font;
                    ritchTB.ForeColor = f1.Color;
                }
            }
        }

        // Función Cambio del color de Fuente
        // Vale tanto para el texto seleccionado como en 'global' por si no hay nada seleccionado
        private void colorTSB_Click(object sender, EventArgs e)
        {
            ColorDialog cd1 = new ColorDialog();

            if (ritchTB.SelectionLength > 0)
            {
                if (cd1.ShowDialog() == DialogResult.OK)
                {
                    ritchTB.ForeColor = cd1.Color;
                }
            }
            //Subraya el contenido completo del 'richtextbox'
            else
            {
                if (cd1.ShowDialog() == DialogResult.OK)
                {
                    ritchTB.ForeColor = cd1.Color;
                }
            }
        }

        //Búsqueda
        // Nos busca por órden la letra o palabra que hemos introducido
        // Al final te dice el número de veces que se encuentra la palabra repetida en el texto
        private void bucquedaTSB_Click(object sender, EventArgs e)
        {
            //Comprobamos que hay palabras escritas en el textBox
            if(busquedaTB.TextLength > 0)
            {                
                //Esto nos sirve a la hora de buscar letras, porque suele saltar el 'break' de una sentencia que nos hará falta
                if (this.contadorIndice < ritchTB.TextLength)
                {
                    //Para buscar por índice (letra por letra) en el richtextBox
                    for (int indexRTB = this.contadorIndice; indexRTB < ritchTB.TextLength; indexRTB++)
                    {
                        String palabra = "";

                        //Comparativa letra a letra del textBox de busqueda y el ritchtextBox
                        // Controlamos también que no se desprenda la posición del índice del ritchTextBox
                        for (int letra = 0; letra < busquedaTB.TextLength && ((indexRTB + letra) < ritchTB.TextLength); letra++)
                        {
                            if (busquedaTB.Text[letra] == ritchTB.Text[indexRTB + letra])
                                palabra += ritchTB.Text[indexRTB + letra];
                            else
                                palabra = "";
                        }

                        this.contadorIndice = indexRTB + 1;

                        //Si la palabra coincide se ejecuta el break y continuamos
                        if (palabra == busquedaTB.Text)
                        {
                            contadorPalabras++;
                            ritchTB.SelectAll();
                            ritchTB.SelectionBackColor = Color.White;
                            ritchTB.Select(indexRTB, busquedaTB.TextLength);
                            ritchTB.SelectionBackColor = Color.LightBlue;
                            break;
                        }

                        // Si el índice ha llegado al final
                        if (indexRTB == ritchTB.TextLength - 1)
                        {
                            MessageBox.Show("Búsqueda completada, coincidencia de palabras: " + this.contadorPalabras, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.contadorIndice = 0;
                            this.contadorPalabras = 0;
                            return; //Paramos la función
                        }
                    }
                    //Caso diferente/Búsqueda de letras
                } else
                {
                    MessageBox.Show("Búsqueda completada, coincidencia de palabras: " + this.contadorPalabras, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.contadorIndice = 0;
                    this.contadorPalabras = 0;
                }
            }
            else
            {
                MessageBox.Show("Inserte previamente un texto","ERROR",MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.contadorIndice = 0;
                this.contadorPalabras = 0;
            }
        }

        //Deshacer
        private void atrasTSB_Click(object sender, EventArgs e)
        {
            ritchTB.Undo();
        }

        //Rehacer
        private void adelanteTSB_Click(object sender, EventArgs e)
        {
            ritchTB.Redo();
        }

        
        //Evento que se ejecuta cada vez que cambia el richTextBox
        private void ritchTB_TextChanged(object sender, EventArgs e)
        {
            controlarLineaRitchTextBox_DeStatusBar();
        }

        
        // Al cerrar/Salir del Formulario/Aplicación general
        private void FormPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            preguntarAntesDeCerrar();
        }

        //-------------------------
        // Métodos auxiliares
        //-------------------------

        //Para controlar el 'StatusStrip' con los datos de la posición del richTextBox
        private void controlarLineaRitchTextBox_DeStatusBar()
        {
            //Linea
            tssLbl.Text = "Línea ";
            int index = ritchTB.SelectionStart;
            tssLblLineaNum.Text = (ritchTB.GetLineFromCharIndex(index)+1).ToString();
            //Columna
            int firstChar = ritchTB.GetFirstCharIndexFromLine(ritchTB.GetLineFromCharIndex(index));
            int column = index - firstChar;
            tssLblc.Text = "Columna ";
            tssLblColumNum.Text = (column+1).ToString();
        }


        //Nos preguntará, por seguridad, si queremos cerrar el formulario al solicitar su cierre
        private void preguntarAntesDeCerrar()
        {
            DialogResult dr = MessageBox.Show("¿Está seguro qué desea cerrar el programa?", "¡ALERTA!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (dr == DialogResult.Yes)
            {
                this.Close();
                Application.Exit();
            }
        }      
    }
}
